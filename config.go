package main

import (
	"fmt"
	"io/ioutil"
	"strings"
	"sync"

	"github.com/pkg/errors"
	"gitlab.com/catastrophic/assistance/fs"
	yaml "gopkg.in/yaml.v2"
)

var config *Config
var onceConfig sync.Once

func NewConfig(path string) (*Config, error) {
	var newConfigErr error
	onceConfig.Do(func() {
		// TODO check path has yamlExt!
		newConf := &Config{}
		if err := newConf.Load(path); err != nil {
			newConfigErr = err
			return
		}
		// set the global pointer once everything is OK.
		config = newConf
	})
	return config, newConfigErr
}

type ConfigBib struct {
	User     string
	Password string
	Cookie   string
}

func (cb *ConfigBib) String() string {
	txt := "Tracker configuration:\n"
	txt += "\tUser: " + cb.User + "\n"
	txt += "\tPassword: " + cb.Password + "\n"
	txt += "\tCookie: " + cb.Cookie + "\n"
	return txt
}

func (cb *ConfigBib) check() error {
	if cb.Cookie == "" && (cb.User == "" || cb.Password == "") {
		return errors.New("tracker configuration is missing information")
	}
	if cb.Cookie != "" && (cb.User != "" || cb.Password != "") {
		fmt.Println("tracker cookie information will be used to log in.")
	}
	return nil
}

type ConfigDirs struct {
	TorrentWatchDir string `yaml:"watch_directory"`
	TorrentSeedDir  string `yaml:"seed_directory"`
	ArchiveDir      string `yaml:"archive_directory"`
}

func (cd *ConfigDirs) String() string {
	txt := "Directories configuration:\n"
	txt += "\tWatch Dir: " + cd.TorrentWatchDir + "\n"
	txt += "\tSeed Dir: " + cd.TorrentSeedDir + "\n"
	txt += "\tArchive Dir: " + cd.ArchiveDir + "\n"
	return txt
}

func (cd *ConfigDirs) check() error {
	if !fs.DirExists(cd.TorrentSeedDir) {
		return errors.New("seed directory does not exist" + cd.TorrentSeedDir)
	}
	if !fs.DirExists(cd.TorrentWatchDir) {
		return errors.New("watch directory does not exist")
	}
	if !fs.DirExists(cd.ArchiveDir) {
		return errors.New("archive directory does not exist")
	}
	return nil
}

type ConfigAliases struct {
	AliasesFile string              `yaml:"aliases_file"`
	Aliases     map[string][]string `yaml:"-"`
}

func (ca *ConfigAliases) String() string {
	txt := "Tags configuration:\n"
	txt += "\tAliases File: " + ca.AliasesFile + "\n"
	for main, aliases := range ca.Aliases {
		txt += "\t - " + main + ": " + strings.Join(aliases, ", ") + "\n"
	}
	return txt
}

// Load the configuration file where the aliases are defined.
func (ca *ConfigAliases) loadMap(path string) (*map[string][]string, error) {
	data, err := ioutil.ReadFile(path)
	if err != nil {
		return nil, err
	}
	m := make(map[string][]string)
	err = yaml.Unmarshal(data, &m)
	if err != nil {
		return nil, err
	}
	return &m, nil
}

func (ca *ConfigAliases) check() error {
	// init
	ca.Aliases = make(map[string][]string)

	if ca.AliasesFile != "" {
		if !fs.FileExists(ca.AliasesFile) {
			return errors.New("aliases file does not exist")
		}
		// load the aliases
		aliases, err := ca.loadMap(ca.AliasesFile)
		if err != nil {
			return errors.Wrap(err, "could not load aliases")
		}
		ca.Aliases = *aliases
	}
	return nil
}

type ConfigGoodReads struct {
	APIKey string `yaml:"api_key"`
}

func (cgr *ConfigGoodReads) String() string {
	txt := "GoodReads configuration:\n"
	txt += "\tAPI Key: " + cgr.APIKey + "\n"
	return txt
}

func (cgr *ConfigGoodReads) check() error {
	if cgr.APIKey == "" {
		return errors.New("goodreads configuration requires API Key. Get one from your account")
	}
	return nil
}

type Config struct {
	Bibliotik   *ConfigBib       `yaml:"bibliotik"`
	Directories *ConfigDirs      `yaml:"directories"`
	Tags        *ConfigAliases   `yaml:"tags"`
	Goodreads   *ConfigGoodReads `yaml:"goodreads"`
}

func (c *Config) Load(file string) error {
	// loading the configuration file
	b, err := ioutil.ReadFile(file)
	if err != nil {
		return err
	}
	return c.LoadFromBytes(b)
}

func (c *Config) LoadFromBytes(b []byte) error {
	err := yaml.Unmarshal(b, &c)
	if err != nil {
		return err
	}
	return c.check()
}

func (c *Config) String() string {
	txt := c.Bibliotik.String() + "\n"
	txt += c.Directories.String() + "\n"
	txt += c.Goodreads.String() + "\n"
	txt += c.Tags.String() + "\n"
	return txt
}

func (c *Config) check() error {
	if err := c.Bibliotik.check(); err != nil {
		return err
	}
	if err := c.Directories.check(); err != nil {
		return err
	}
	if err := c.Goodreads.check(); err != nil {
		return err
	}
	if err := c.Tags.check(); err != nil {
		return err
	}
	return nil
}
