package main

import (
	"fmt"
	"net/http"
	"os"
	"path/filepath"
	"strings"

	docopt "github.com/docopt/docopt-go"
	"github.com/pkg/errors"
	"gitlab.com/catastrophic/assistance/fs"
)

const (
	usage = `
	 __   __       ___  ___ 
	|__) |__) |  |  |  |__  
	|__) |  \ \__/  |  |___ 
                        
	
Description:
    
	BRUTE stands for: Bibliotik Retail Uploader Torrent Epubs.
	It does not mean anything, but brutes are not known to be particularly
	articulate.

	In short, BRUTE allows uploading a retail epub to Bib from the comfort of the
	command line.

	BRUTE retrieves metadata from the epub and Goodreads automatically, and forces
	the user to merge and edit the results as needed.
	Everything is saved to a json file, for (optional, and encouraged) manual editing
	before the actual upload.
	
Usage:
    brute [--config=<CONFIG>] <EPUB> [<COVER_URL>] 

Options:
    --config=<CONFIG>      Use a specific configuration file.
    -h, --help             Show this screen.
    --version              Show version.
`
	fullName    = "brute"
	fullVersion = "%s -- %s"
)

var (
	Version = "dev"
)

type bruteArgs struct {
	builtin           bool
	epubFile          string
	coverURL          string
	coverFile         string
	configurationFile string
}

func (b *bruteArgs) parseCLI(osArgs []string) error {
	// parse arguments and options
	args, err := docopt.Parse(usage, osArgs, true, fmt.Sprintf(fullVersion, fullName, Version), false, false)
	if err != nil {
		return errors.Wrap(err, "incorrect arguments")
	}
	if len(args) == 0 {
		// builtin command, nothing to do.
		b.builtin = true
		return nil
	}

	if args["--config"] != nil {
		b.configurationFile = args["--config"].(string)
		if !fs.FileExists(b.configurationFile) {
			return errors.New("alternative configuration file does not exist")
		}
	}

	b.epubFile = args["<EPUB>"].(string)
	if !isValidEPUBFile(b.epubFile) {
		return errors.New("this does not seem to be a valid epub file")
	}

	url, ok := args["<COVER_URL>"].(string)
	if ok {
		b.coverURL = url
		// temp format until we know the format
		coverFileTemp := strings.Replace(b.epubFile, filepath.Ext(b.epubFile), ".image", 1)
		if err := fs.DownloadFile(coverFileTemp, b.coverURL); err != nil {
			return errors.Wrap(err, "could not fetch cover")
		}

		f, err := os.Open(coverFileTemp)
		if err != nil {
			panic(err)
		}
		defer f.Close()

		buffer := make([]byte, 512)
		if _, err := f.Read(buffer); err != nil {
			return err
		}
		switch http.DetectContentType(buffer) {
		case "image/jpeg":
			b.coverFile = strings.Replace(coverFileTemp, ".image", ".jpg", 1)
		case "image/png":
			b.coverFile = strings.Replace(coverFileTemp, ".image", ".png", 1)
		case "image/gif":
			b.coverFile = strings.Replace(coverFileTemp, ".image", ".gif", 1)
		default:
			return errors.New("unknown image format: " + http.DetectContentType(buffer))
		}
		if err := os.Rename(coverFileTemp, b.coverFile); err != nil {
			return err
		}
	}
	return nil
}
