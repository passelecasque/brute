package main

import (
	"fmt"
	"os"

	"gitlab.com/catastrophic/assistance/ui"
	"gitlab.com/passelecasque/obstruction/tracker"
)

const (
	UploadAnonymously = "Do you want the upload to be anonymous"

	AllInfoRetrievedLastChanceToEdit = "All relevant information has been retrieved.\n" +
		"If you are satisfied everything is perfect, you can upload directly.\n" +
		"If unsure, it is not too late to fix things manually."
	SearchingForDupes = "Now checking if the book you want to upload already exists on the tracker.\n" +
		"If something is found, check the links to make sure you are not uploading a duplicate" +
		"(check the official rules about what is and what is not considered a dupe).\n" +
		"Search uses exact author and title only, it may not find relevant books if your information " +
		"is different from existing books on the tracker, or show different editions."
	ConfirmNotADupe = "After checking, are you certain you can upload this book"
)

func main() {
	// parsing CLI
	cli := &bruteArgs{}
	if err := cli.parseCLI(os.Args[1:]); err != nil {
		fmt.Println(err.Error())
		return
	}
	if cli.builtin {
		return
	}

	configurationFile := "config.yaml"
	if cli.configurationFile != "" {
		configurationFile = cli.configurationFile
	}

	c, err := NewConfig(configurationFile)
	if err != nil {
		fmt.Println(err.Error())
		return
	}

	u, err := NewUpload(cli.epubFile, c.Goodreads.APIKey, cli.coverURL, cli.coverFile)
	if err != nil {
		fmt.Println(err.Error())
		return
	}
	// load metadata, using previously saved info if available
	if err := u.LoadMetadata(); err != nil {
		fmt.Println(err.Error())
		return
	}
	// get GR metadata and merge with current metadata
	if ui.Accept("Compare and merge with Goodreads metadata") {
		if err := u.MergeWithOnlineInfo(c.Tags.Aliases); err != nil {
			fmt.Println(err.Error())
			return
		}
	}
	// is it retail
	if !u.LoadedFromFile && ui.Accept("Is this retail") {
		u.Retail = true
	}
	// anonymous?
	if !u.LoadedFromFile && ui.Accept(UploadAnonymously) {
		u.Anonymous = true
	}

	// save current metadata
	if err := u.SaveMetadata(); err != nil {
		fmt.Println(err.Error())
		return
	}
	ui.Usage(AllInfoRetrievedLastChanceToEdit)

	// prepare the upload form and show what we currently have
	uf := u.ToUploadForm()
	ui.Header("Final Upload form")
	fmt.Println(uf.ShowInfo())

	// last chance!
	if !ui.Accept("Proceed and upload the epub") {
		fmt.Println(ui.GreenBold("Feel free to edit ") + ui.YellowUnderlined(u.JSONFilename) + ui.GreenBold(" and restart again."))
		return
	}

	// log in & search for duplicates
	b, err := tracker.NewBib("https://bibliotik.me", c.Bibliotik.User, c.Bibliotik.Password, c.Bibliotik.Cookie, fullName+"/"+Version)
	if err != nil {
		fmt.Println(err.Error())
		return
	}
	go b.RateLimiter()

	if err := b.Login(); err != nil {
		fmt.Println(err.Error())
		return
	}
	if err := b.GetAnnounceURL(); err != nil {
		fmt.Println(err.Error())
		return
	}

	// search for similar things
	ui.Header("Searching for duplicates")
	ui.Usage(SearchingForDupes)
	hits, err := b.Search(u.Metadata.Author(), u.Metadata.Title)
	if err != nil {
		fmt.Println(err.Error())
		return
	}
	if len(hits) != 0 {
		fmt.Println("Found the following existing release(s): ")
		for _, h := range hits {
			fmt.Println(h)
		}
		if !ui.Accept(ConfirmNotADupe) {
			fmt.Println(ui.RedBold("Nothing was uploaded."))
			return
		}
	} else {
		fmt.Println(ui.Yellow("Nothing similar could be found on the tracker already. Proceeding."))
	}

	// ok, here everything should be ready and we're pretty sure this is not a duplicate.
	// prepare files for seeding
	ui.Header("Uploading")
	// generate torrent
	if err := u.GenerateTorrent("life finds a way", "BIB", b.Passkey); err != nil {
		fmt.Println(err.Error())
		return
	}
	// copy for seeding
	if err := u.Seed(c.Directories.TorrentWatchDir, c.Directories.TorrentSeedDir); err != nil {
		fmt.Println(err.Error())
		return
	}

	// upload!
	if err := b.Upload(uf); err != nil {
		fmt.Println(err.Error())
		return
	}

	ui.Header("Archiving files")
	// archive everything and optionally remove original files
	if err := u.Archive(c.Directories.ArchiveDir); err != nil {
		fmt.Println(err.Error())
		return
	}
}
