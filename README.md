# BRUTE

[![Documentation](https://godoc.org/gitlab.com/passelecasque/brute?status.svg)](https://godoc.org/gitlab.com/passelecasque/brute)
 [![Go Report Card](https://goreportcard.com/badge/gitlab.com/passelecasque/brute)](https://goreportcard.com/report/gitlab.com/passelecasque/brute) [![codecov](https://codecov.io/gl/passelecasque/brute/branch/master/graph/badge.svg)](https://codecov.io/gl/passelecasque/brute) [![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT) 
[![Build status](https://gitlab.com/passelecasque/brute/badges/master/build.svg)](https://gitlab.com/passelecasque/brute/pipelines)
