package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"
	"time"

	"github.com/mholt/archiver"

	"github.com/anacrolix/torrent/bencode"
	"github.com/anacrolix/torrent/metainfo"
	"github.com/pkg/errors"
	"gitlab.com/catastrophic/assistance/ebook"
	"gitlab.com/catastrophic/assistance/epub"
	"gitlab.com/catastrophic/assistance/fs"
	"gitlab.com/catastrophic/assistance/strslice"
	"gitlab.com/catastrophic/assistance/ui"
	"gitlab.com/passelecasque/obstruction/tracker"
)

const (
	BRUTE = "BRUTE"

	EPUBExtension = ".epub"

	EPUBFormat      = "15"
	EnglishLanguage = "1"
	GermanLanguage  = "2"
	FrenchLanguage  = "3"
	SpanishLanguage = "4"
	ItalianLanguage = "5"
)

type Upload struct {
	Filename       string          `json:"epub_filename"`
	JSONFilename   string          `json:"-"`
	Cover          string          `json:"cover_url"`
	CoverFile      string          `json:"cover_file"`
	CoverFileGR    string          `json:"cover_file_gr"`
	Torrent        string          `json:"-"`
	Epub           *epub.Epub      `json:"-"`
	Metadata       *ebook.Metadata `json:"metadata"`
	Retail         bool            `json:"retail"`
	Notify         bool            `json:"notify"`
	Anonymous      bool            `json:"anonymous"`
	LoadedFromFile bool            `json:"-"`
}

func isValidEPUBFile(src string) bool {
	//  assert epub exists
	if !fs.FileExists(src) {
		return false
	}
	// assert it's an epub
	if !strings.HasSuffix(strings.ToLower(src), EPUBExtension) {
		return false
	}
	return true
}

func NewUpload(file, grKey, cover, coverFile string) (*Upload, error) {
	jsonFilename := strings.Replace(file, filepath.Ext(file), ".json", 1)
	torrentFilename := strings.Replace(file, filepath.Ext(file), ".torrent", 1)
	coverFileGR := strings.Replace(file, filepath.Ext(file), ".GR.jpg", 1)
	u := &Upload{Filename: file, JSONFilename: jsonFilename, Torrent: torrentFilename, Cover: cover, CoverFile: coverFile, CoverFileGR: coverFileGR, Notify: true}
	e, err := epub.NewWithGoodReads(file, grKey)
	if err != nil {
		return nil, err
	}
	u.Epub = e
	return u, nil
}

func (u *Upload) LoadMetadataFromFile(file string) error {
	data, err := ioutil.ReadFile(file)
	if err != nil {
		return errors.Wrap(err, "error loading metadata")
	}
	return json.Unmarshal(data, &u)
}

func (u *Upload) LoadMetadata() error {
	if fs.FileExists(u.JSONFilename) {
		// saving cover in case the json does not have one (provided by CLI)
		var cover string
		if u.Cover != "" {
			cover = u.Cover
		}
		if err := u.LoadMetadataFromFile(u.JSONFilename); err != nil {
			return err
		}
		u.Epub.Metadata = u.Metadata
		// overwriting cover if necessary
		if cover != "" {
			u.Cover = cover
		}
		u.LoadedFromFile = true
		ui.Header("Loaded Metadata")
	} else {
		if err := u.Epub.Parse(); err != nil {
			return err
		}
		if err := u.Epub.ReadMetadata(true, false); err != nil {
			return err
		}
		u.Metadata = u.Epub.Metadata
		ui.Header("Epub Metadata")
	}
	fmt.Println(u.Epub.ShowInfo())
	return nil
}

func (u *Upload) MergeWithOnlineInfo(tagAliases map[string][]string) error {
	if err := u.Epub.GetOnlineMetadata(true, nil, tagAliases); err != nil {
		return err
	}
	u.Metadata = u.Epub.Metadata
	// downloading cover from GR
	u.CoverFileGR = strings.Replace(u.Filename, filepath.Ext(u.Filename), ".GR.jpg", 1)
	if err := fs.DownloadFile(u.CoverFileGR, u.Metadata.ImageURL); err != nil {
		return errors.Wrap(err, "could not fetch cover from GR")
	}
	if u.Cover == "" || ui.Accept("Would you rather use the GR cover") {
		u.Cover = u.Metadata.ImageURL
	}

	ui.Header("Final Metadata")
	fmt.Println(u.Epub.ShowInfo())
	return nil
}

func (u *Upload) SaveMetadata() error {
	data, err := json.MarshalIndent(u, "  ", "    ")
	if err != nil {
		return err
	}
	return ioutil.WriteFile(u.JSONFilename, data, 0666)
}

// newTrue allows setting the value of a *bool in a struct.
// it is arguably awful.
func newTrue() *bool {
	b := true
	return &b
}

func (u *Upload) GenerateTorrent(comment, source, passkey string) error {
	fmt.Println("Building torrent file " + filepath.Base(u.Torrent))
	mi := metainfo.MetaInfo{Announce: passkey}
	mi.Comment = comment
	mi.CreatedBy = BRUTE
	mi.CreationDate = time.Now().Unix()
	i := metainfo.Info{PieceLength: 256 * 1024, Private: newTrue(), Source: source}
	if err := i.BuildFromFilePath(u.Filename); err != nil {
		return err
	}
	bytes, err := bencode.Marshal(i)
	if err != nil {
		return err
	}
	mi.InfoBytes = bytes
	f, err := os.Create(u.Torrent)
	if err != nil {
		return err
	}
	defer f.Close()
	return mi.Write(f)
}

func (u *Upload) Seed(watchDir, seedDir string) error {
	fmt.Println("Copying files to begin seeding.")
	// copy epub to seed dir
	seedPath := filepath.Join(seedDir, filepath.Base(u.Filename))
	if !fs.FileExists(seedPath) {
		if err := fs.CopyFile(u.Filename, seedPath, false); err != nil {
			return err
		}
	} else {
		fmt.Println("Epub already in seed directory.")
	}
	// copy torrent to watch dir
	if err := fs.CopyFile(u.Torrent, filepath.Join(watchDir, filepath.Base(u.Torrent)), false); err != nil {
		return err
	}
	return nil
}

func (u *Upload) ToUploadForm() *tracker.BibUploadForm {
	uf := tracker.NewDefaultBibUploadForm()

	// filling all fields
	uf.TorrentFileField = u.Torrent

	// title contains series information
	seriesInfo := ""
	if u.Metadata.Series.HasAny() {
		for i := range u.Metadata.Series {
			seriesInfo += fmt.Sprintf(" (%s, Book %s)", u.Metadata.Series[i].Name, u.Metadata.Series[i].Position)
		}
	}
	uf.TitleField = u.Metadata.Title + seriesInfo
	uf.EditorsField = u.Metadata.Editor()
	uf.ContributorsField = u.Metadata.Contributor()
	uf.TranslatorsField = u.Metadata.Translator()
	// some publishers in the form 'X, Inc' make the upload form angry
	uf.PublishersField = strings.Replace(u.Metadata.Publisher, ",", " ", -1)
	uf.PagesField = u.Metadata.NumPages
	if u.Metadata.Author() != "Unknown" {
		uf.AuthorsField = u.Metadata.Author()
	}
	uf.FormatField = EPUBFormat
	uf.IsbnField = u.Metadata.ISBN

	// concatenating category, genre, and tags
	tags := []string{u.Metadata.Category, u.Metadata.Genre}
	tags = append(tags, u.Metadata.Tags.Slice()...)
	strslice.RemoveDuplicates(&tags)
	uf.TagsField = strings.Join(tags, ",")

	// description with basic formatting
	var replacements []string
	for _, a := range u.Metadata.Authors {
		replacements = append(replacements, a.Name)
		replacements = append(replacements, "**"+a.Name+"**")
	}
	replacements = append(replacements, u.Metadata.Title)
	replacements = append(replacements, "*"+u.Metadata.Title+"*")

	r := strings.NewReplacer(replacements...)
	uf.DescriptionField = r.Replace(u.Metadata.Description)

	uf.YearField = u.Metadata.EditionYear
	uf.ImageField = u.Cover

	uf.RetailField = "0"
	if u.Retail {
		uf.RetailField = "1"
	}
	uf.NotifyField = "0"
	if u.Notify {
		uf.NotifyField = "1"
	}
	switch u.Metadata.Language {
	case "en":
		uf.LanguageField = EnglishLanguage
	case "fr":
		uf.LanguageField = FrenchLanguage
	case "de":
		uf.LanguageField = GermanLanguage
	case "it":
		uf.LanguageField = ItalianLanguage
	case "es":
		uf.LanguageField = SpanishLanguage
	}
	uf.AnonymousField = "0"
	if u.Anonymous {
		uf.AnonymousField = "1"
	}
	uf.SupplementsField = "0"
	return uf
}

func (u *Upload) Archive(archiveDir string) error {
	// generate archive name
	currentTime := time.Now().Local()
	currentDay := currentTime.Format("2006-01-02")
	archiveName := filepath.Join(archiveDir, fmt.Sprintf("%s - %s - %s (%s) %s.tar.gz", currentDay, u.Metadata.ISBN, u.Metadata.Author(), u.Metadata.EditionYear, u.Metadata.Title))
	if fs.FileExists(archiveName) {
		return errors.New("Archive " + archiveName + " already exists")
	}
	fmt.Println("Backing up files to " + filepath.Base(archiveName))

	// create the archive
	t := archiver.NewTarGz()
	files := []string{u.Filename, u.Torrent, u.JSONFilename}
	if fs.FileExists(u.CoverFile) {
		files = append(files, u.CoverFile)
	}
	if fs.FileExists(u.CoverFileGR) {
		files = append(files, u.CoverFileGR)
	}
	if err := t.Archive(files, archiveName); err != nil {
		return err
	}
	if ui.Accept("Remove original files") {
		for _, f := range files {
			if err := os.Remove(f); err != nil {
				return err
			}
		}
	}
	return nil
}
