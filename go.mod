module gitlab.com/passelecasque/brute

require (
	github.com/anacrolix/torrent v1.0.1
	github.com/docopt/docopt-go v0.0.0-20180111231733-ee0de3bc6815
	github.com/dsnet/compress v0.0.0-20171208185109-cc9eb1d7ad76 // indirect
	github.com/mholt/archiver v3.1.1+incompatible
	github.com/nwaples/rardecode v1.0.0 // indirect
	github.com/pierrec/lz4 v2.0.5+incompatible // indirect
	github.com/pkg/errors v0.8.1
	github.com/ulikunitz/xz v0.5.6 // indirect
	github.com/xi2/xz v0.0.0-20171230120015-48954b6210f8 // indirect
	gitlab.com/catastrophic/assistance v0.36.2
	gitlab.com/passelecasque/obstruction v0.1.0
	gopkg.in/yaml.v2 v2.2.8
)

go 1.13

// replace gitlab.com/catastrophic/assistance => ../../catastrophic/assistance
